import React from "react";
import { GoogleMap, Marker, useJsApiLoader } from "@react-google-maps/api";

const mapContainerStyle = {
  width: "100%",
  height: "400px",
};

const center = {
  lat: 0,
  lng: 0,
};

const LocationViewer = ({ lat, lng }) => {
  console.log("Latitudine:", lat); 
  console.log("Longitudine:", lng); 

  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: "AIzaSyDlmzLF-y_UdrFGND3YelN0BXCoDZSyvPM",
  });

  if (isNaN(lat) || isNaN(lng)) {
    return <div>Location unspecified.</div>;
  }

  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={mapContainerStyle}
      center={{ lat, lng }}
      zoom={15}
    >
      <Marker position={{ lat, lng }} />
    </GoogleMap>
  ) : (
    <></>
  );
};

export default LocationViewer;
