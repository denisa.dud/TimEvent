import React, { useState, useRef, useContext, useReducer, useEffect } from "react";
import { Avatar, Button, Alert, Popover, PopoverHandler, PopoverContent } from "@material-tailwind/react";
import avatar from "../../assets/avatar.png";
import { AuthContext } from "../AppContext/AppContext";
import { doc, setDoc, collection, serverTimestamp, query, orderBy, onSnapshot, getDocs, where } from "firebase/firestore";
import { db } from "../Firebase/firebase";
import { PostsReducer, postActions, postsStates } from "../AppContext/PostReducer";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import PostCard from "./PostCard";
import { format } from "date-fns";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/outline";
import { DayPicker } from "react-day-picker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt} from "@fortawesome/free-solid-svg-icons"; 
import MapPicker from "../Pages/MapPicker";
import { faCamera, faPlusCircle } from "@fortawesome/free-solid-svg-icons";


const Main = () => {
  const { user, userData } = useContext(AuthContext);
  const text = useRef("");
  const scrollRef = useRef("");
  const [image, setImage] = useState(null);
  const [file, setFile] = useState(null);
  const [location, setLocation] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const collectionRef = collection(db, "posts");
  const postRef = doc(collection(db, "posts"));
  const document = postRef.id;
  const [state, dispatch] = useReducer(PostsReducer, postsStates);
  const { SUBMIT_POST, HANDLE_ERROR } = postActions;
  const [progressBar, setProgressBar] = useState(0);
  const [isOrganizer, setIsOrganizer] = useState(false);
  const [mapPickerVisible, setMapPickerVisible] = useState(false);
  const organizersRef = collection(db, "organizers");
  const [organizerData, setOrganizerData] = useState({});
  const [showNotification, setShowNotification] = useState(false); 

  useEffect(() => {
    if (user) {
      const unsubscribe = onSnapshot(
        query(collection(db, "organizers"), where("uid", "==", user.uid)),
        (snapshot) => {
          snapshot.forEach((doc) => {
            const data = doc.data();
            setOrganizerData(data);
          });
        }
      );

      return () => {
        unsubscribe();
      };
    }
  }, [user]);
  
  useEffect(() => {
    const checkOrganizerStatus = async () => {
      try {
        const organizersSnapshot = await getDocs(organizersRef);
        const organizersData = organizersSnapshot.docs.map(doc => doc.data());
        const currentUserIsOrganizer = organizersData.some(organizer => organizer.uid === user.uid);
        setIsOrganizer(currentUserIsOrganizer);
      } catch (error) {
        console.error("Error checking organizer status:", error);
      }
    };

    if (user) {
      checkOrganizerStatus();
    }
  }, [user]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowNotification(false);
    }, 5000); 

    return () => clearTimeout(timer);
  }, [showNotification]);

  const handleUpload = (e) => {
    setFile(e.target.files[0]);
  };

  const handleSubmitPost = async (e) => {
    e.preventDefault();
    if (text.current.value !== "") {
      try {
        const organizersSnapshot = await getDocs(organizersRef);
        const organizerData = organizersSnapshot.docs.find(
          (doc) => doc.data().uid === user.uid
        )?.data();
  
        if (organizerData) {
          await setDoc(postRef, {
            documentId: document,
            uid: organizerData.uid,
            name: organizerData.displayName || organizerData.name,
            email: organizerData.email,
            text: text.current.value,
            image: image,
            timestamp: serverTimestamp(),
            selectedDate: selectedDate,
            location: location, 
          });
          text.current.value = "";
          setSelectedDate(null);
          setLocation(null); 
          setShowNotification(true);
        } else {
          console.error("Organizer data not found.");
        }
      } catch (err) {
        dispatch({ type: HANDLE_ERROR });
        alert(err.message);
        console.log(err.message);
      }
    } else {
      dispatch({ type: HANDLE_ERROR });
    }
  };
  
  const storage = getStorage();

  const metadata = {
    contentType: [
      "image/jpeg",
      "image/jpg",
      "image/png",
      "image/gif",
      "image/svg+xml",
    ],
  };

  const submitImage = async () => {
    const fileType = metadata.contentType.includes(file["type"]);
    if (!file) return;
    if (fileType) {
      try {
        const storageRef = ref(storage, `images/${file.name}`);
        const uploadTask = uploadBytesResumable(
          storageRef,
          file,
          metadata.contentType
        );
        await uploadTask.on(
          "state_changed",
          (snapshot) => {
            const progress = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgressBar(progress);
          },
          (error) => {
            alert(error);
          },
          async () => {
            await getDownloadURL(uploadTask.snapshot.ref).then(
              (downloadURL) => {
                setImage(downloadURL);
              }
            );
          }
        );
      } catch (err) {
        dispatch({ type: HANDLE_ERROR });
        alert(err.message);
        console.log(err.message);
      }
    }
  };

  const [allSelectedDates, setAllSelectedDates] = useState([]);

  useEffect(() => {
    const postData = async () => {
      const q = query(collectionRef, orderBy("timestamp", "desc"));
      await onSnapshot(q, (doc) => {
        const newPosts = doc?.docs?.map((item) => item?.data());
        dispatch({
          type: SUBMIT_POST,
          posts: newPosts,
        });
  
        
        const selectedDatesFromPosts = newPosts.map(post => post.selectedDate);
        setAllSelectedDates(selectedDatesFromPosts);
  
        setImage(null);
        setFile(null);
        setProgressBar(0);
      });
    };
  
    postData();
  
    return () => postData();
  }, [SUBMIT_POST]);
  

  return (
    <div className="flex flex-col items-center">
      {isOrganizer && (
      <div className="flex flex-col py-4 w-full bg-white rounded-3xl shadow-lg">
        <div className="flex items-center border-b-2 border-gray-300 pb-4 pl-4 w-full">
          <Avatar
            size="sm"
            variant="circular"
            src={ organizerData?.profileImage || userData?.profileImage ||  user?.photoURL || avatar}
            alt="avatar"
          ></Avatar>
          <form className="w-full" onSubmit={handleSubmitPost}>
            <div className="flex justify-between items-center">
              <div className="w-full ml-4">
                <input
                  type="text"
                  name="text"
                  placeholder={`Post your event!`}
                  className="outline-none w-full bg-white rounded-md"
                  ref={text}
                ></input>
              </div>
              <div className="mx-4">
                {image && (
                  <img
                    className="h-24 rounded-xl"
                    src={image}
                    alt="previewImage"
                  ></img>
                )}
              </div>
              <div className="mr-4">
                <Button variant="text" type="submit">
                  Share
                </Button>
              </div>
            </div>
          </form>
        </div>
        <span
          style={{ width: `${progressBar}%` }}
          className="bg-blue-700 py-1 rounded-md"
        ></span>
        <div className="flex justify-around items-center pt-4">
        <div className="flex items-center">
  <label htmlFor="addImage" className="cursor-pointer flex items-center">
    <FontAwesomeIcon icon={faCamera} className="h-6 w-6 text-gray-700" />
    <FontAwesomeIcon icon={faPlusCircle} className="h-4 w-4 text-gray-700 ml-1" />
    <input id="addImage" type="file" style={{ display: "none" }} onChange={handleUpload}></input>
  </label>
  {file && (
    <Button variant="text" onClick={submitImage}>
      Upload
    </Button>
  )}
</div>
          <Button ripple="light" className="hover:bg-transparent hover:text-gray-900 hover:shadow-none" onClick={() => setMapPickerVisible(true)}>
            Pick a location
          </Button>
          <div className="flex items-center">
            <Popover placement="bottom">
              <PopoverHandler>
                <Button
                  ripple="light"
                  className="hover:bg-transparent hover:text-gray-900 hover:shadow-none"
                >
                  <FontAwesomeIcon icon={faCalendarAlt} className="mr-2" />
                  {selectedDate ? format(selectedDate, "dd/MM/yyyy") : "Pick a date"}
                </Button>
              </PopoverHandler>
              <PopoverContent>
                <DayPicker
                  mode="single"
                  selected={selectedDate}
                  onSelect={setSelectedDate}
                  showOutsideDays
                  className="border-0"
                  classNames={{
                    caption: "flex justify-center py-2 mb-4 relative items-center",
                    caption_label: "text-sm font-medium text-gray-900",
                    nav: "flex items-center",
                    nav_button:
                      "h-6 w-6 bg-transparent hover:bg-blue-gray-50 p-1 rounded-md transition-colors duration-300",
                    nav_button_previous: "absolute left-1.5",
                    nav_button_next: "absolute right-1.5",
                    table: "w-full border-collapse",
                    head_row: "flex font-medium text-gray-900",
                    head_cell: "m-0.5 w-9 font-normal text-sm",
                    row: "flex w-full mt-2",
                    cell: "text-gray-600 rounded-md h-9 w-9 text-center text-sm p-0 m-0.5 relative [&:has([aria-selected].day-range-end)]:rounded-r-md [&:has([aria-selected].day-outside)]:bg-gray-900/20 [&:has([aria-selected].day-outside)]:text-white [&:has([aria-selected])]:bg-gray-900/50 first:[&:has([aria-selected])]:rounded-l-md last:[&:has([aria-selected])]:rounded-r-md focus-within:relative focus-within:z-20",
                    day: "h-9 w-9 p-0 font-normal",
                    day_range_end: "day-range-end",
                    day_selected:
                      "rounded-md bg-gray-900 text-white hover:bg-gray-900 hover:text-white focus:bg-gray-900 focus:text-white",
                    day_today: "rounded-md bg-gray-200 text-gray-900",
                    day_outside:
                      "day-outside text-gray-500 opacity-50 aria-selected:bg-gray-500 aria-selected:text-gray-900 aria-selected:bg-opacity-10",
                    day_disabled: "text-gray-500 opacity-50",
                    day_hidden: "invisible",
                  }}
                  components={{
                    IconLeft: ({ ...props }) => (
                      <ChevronLeftIcon {...props} className="h-4 w-4 stroke-2" />
                    ),
                    IconRight: ({ ...props }) => (
                      <ChevronRightIcon {...props} className="h-4 w-4 stroke-2" />
                    ),
                  }}
                />
              </PopoverContent>
            </Popover>
          </div>
        </div>
        {mapPickerVisible && (
          <MapPicker onSelectLocation={(location) => {
            console.log("Selected location:", location);
            setMapPickerVisible(false);

            // Actualizează locația în stare
            setLocation(location);
          }} />
        )}
      </div>
      )}
      <div className="flex flex-col py-4 w-full">
        {showNotification && (
          <div className="flex justify-center items-center">
            <Alert>A new post has been added!</Alert>
          </div>
        )}
        {state?.error ? (
          <div className="flex justify-center items-center">
            <Alert color="red">
              Something went wrong refresh and try again...
            </Alert>
          </div>
        ) : (
          <div>
            {state?.posts?.length > 0 &&
              state?.posts?.map((post, index) => {
                return (
                  <PostCard
                    key={index}
                    logo={post?.logo}
                    id={post?.documentId}
                    uid={post?.uid}
                    name={post?.name}
                    email={post?.email}
                    image={post?.image}
                    text={post?.text}
                    location={post?.location}
                    selectedDate={new Date(
                      post?.selectedDate?.toDate()
                    )?.toUTCString()}
                    timestamp={new Date(
                      post?.timestamp?.toDate()
                    )?.toUTCString()}
                  ></PostCard>
                );
              })}
          </div>
        )}
      </div>
      <div ref={scrollRef}>{/* refference for later */}</div>
    </div>
  );
};

export default Main;
