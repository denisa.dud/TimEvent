import React, { useContext, useRef, useReducer, useEffect, useState } from "react";
import { Avatar } from "@material-tailwind/react";
import avatar from "../../assets/avatar.png";
import { AuthContext } from "../AppContext/AppContext";
import {
  setDoc,
  collection,
  doc,
  serverTimestamp,
  orderBy,
  query,
  onSnapshot,
} from "firebase/firestore";
import { db } from "../Firebase/firebase";
import {
  PostsReducer,
  postActions,
  postsStates,
} from "../AppContext/PostReducer";
import Comment from "./Comment";

const CommentSection = ({ postId }) => {
  const commentRef = useRef("");
  const { user, userData } = useContext(AuthContext);
  const commentCollectionRef = collection(db, "posts", postId, "comments");
  const [state, dispatch] = useReducer(PostsReducer, postsStates);
  const { ADD_COMMENT, HANDLE_ERROR } = postActions;

  const addComment = async (e) => {
    e.preventDefault();
    if (commentRef.current.value !== "") {
      try {
        await setDoc(doc(commentCollectionRef), {
          comment: commentRef.current.value,
          image: userData?.profileImage || user?.photoURL || avatar,
          name: userData?.name || user?.displayName || "Anonymous",
          timestamp: serverTimestamp(),
        });
        commentRef.current.value = "";
      } catch (err) {
        dispatch({ type: HANDLE_ERROR });
        alert(err.message);
        console.log(err.message);
      }
    }
  };

  useEffect(() => {
    const getComments = async () => {
      try {
        const q = query(commentCollectionRef, orderBy("timestamp", "desc"));
        onSnapshot(q, (snapshot) => {
          dispatch({
            type: ADD_COMMENT,
            comments: snapshot.docs.map((doc) => doc.data()),
          });
        });
      } catch (err) {
        dispatch({ type: HANDLE_ERROR });
        alert(err.message);
        console.log(err.message);
      }
    };

    getComments();
  }, [postId, ADD_COMMENT, HANDLE_ERROR]);

  return (
    <div className="flex flex-col bg-white w-full py-2 rounded-b-3xl">
      <div className="flex items-center">
        <div className="mx-2">
          <Avatar
            size="sm"
            variant="circular"
            src={userData?.profileImage || user?.photoURL || avatar}
          ></Avatar>
        </div>
        <div className="w-full pr-2">
          <form className="flex items-center w-full" onSubmit={addComment}>
            <input
              name="comment"
              type="text"
              placeholder="Write a comment..."
              className="w-full rounded-2xl outline-none border-0 p-2 bg-gray-100"
              ref={commentRef}
            ></input>
            <button className="hidden" type="submit">
              Submit
            </button>
          </form>
        </div>
      </div>
      {state?.comments?.map((comment, index) => (
        <Comment
          key={index}
          image={comment.image}
          name={comment.name}
          comment={comment.comment}
        />
      ))}
    </div>
  );
};

export default CommentSection;

