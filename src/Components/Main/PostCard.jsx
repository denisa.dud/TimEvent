import React, { useState, useContext, useEffect, useReducer } from "react";
import { Avatar, Dialog, DialogBody, DialogFooter, DialogHeader, Button } from "@material-tailwind/react";
import avatar from "../../assets/avatar.png";
import like from "../../assets/like.png";
import comment from "../../assets/comment.png";
import remove from "../../assets/delete.png";
import addFriend from "../../assets/add-friend.png";
import participants from "../../assets/participants.png";
import { AuthContext } from "../AppContext/AppContext";
import { PostsReducer, postActions, postsStates } from "../AppContext/PostReducer";
import { doc, setDoc, addDoc, getDoc, collection, query, onSnapshot, where, getDocs, updateDoc, arrayUnion, deleteDoc } from "firebase/firestore";
import { db } from "../Firebase/firebase";
import CommentSection from "./CommentSection";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkedAlt } from "@fortawesome/free-solid-svg-icons";
import LocationViewer from "./LocationViewer";

const PostCard = ({ uid, id, logo, name, email, text, image, timestamp, selectedDate, location }) => {
  const { user, userData } = useContext(AuthContext);
  const [state, dispatch] = useReducer(PostsReducer, postsStates);
  const likesRef = doc(collection(db, "posts", id, "likes"));
  const likesCollection = collection(db, "posts", id, "likes");
  const singlePostDocument = doc(db, "posts", id);
  const { ADD_LIKE, HANDLE_ERROR } = postActions;
  const [openLikesDialog, setOpenLikesDialog] = useState(false);
  const [openCommentSection, setOpenCommentSection] = useState(false);
  const [likers, setLikers] = useState([]);
  const [postLocation, setPostLocation] = useState(null);
  const [showLocationDialog, setShowLocationDialog] = useState(false);
  const [organizerData, setOrganizerData] = useState({});

  const handleOpenLikesDialog = () => setOpenLikesDialog(!openLikesDialog);
  const toggleLocationDialog = () => setShowLocationDialog(!showLocationDialog);

  const addUser = async () => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const querySnapshot = await getDocs(q);
      if (!querySnapshot.empty) {
        const docRef = querySnapshot.docs[0].ref;
        await updateDoc(docRef, {
          friends: arrayUnion({
            id: uid,
            image: logo || "",
            name: name || "",
          }),
        });
      } else {
        console.log("User document not found");
      }
    } catch (err) {
      alert(err.message);
      console.error(err);
    }
  };

  useEffect(() => {
    const getOrganizerData = async () => {
      try {
        const organizerQuery = query(collection(db, "organizers"), where("uid", "==", uid));
        const organizerSnapshot = await getDocs(organizerQuery);
        if (!organizerSnapshot.empty) {
          const organizer = organizerSnapshot.docs[0].data();
          setOrganizerData(organizer);
        }
      } catch (error) {
        console.error("Error getting organizer data:", error);
      }
    };

    getOrganizerData();
  }, [uid]);

  useEffect(() => {
    const getLocationData = async () => {
      try {
        const postDocRef = doc(db, "posts", id);
        const postDocSnapshot = await getDoc(postDocRef);
        if (postDocSnapshot.exists()) {
          const postData = postDocSnapshot.data();
          if (postData.location) {
            setPostLocation(postData.location);
          }
        } else {
          console.log("No such document!");
        }
      } catch (error) {
        console.error("Error getting document:", error);
      }
    };

    getLocationData();
  }, [id]);

  const handleLike = async (e) => {
    e.preventDefault();
    console.log("handleLike triggered");
    const q = query(likesCollection, where("id", "==", user?.uid));
    const querySnapshot = await getDocs(q);
    const likesDocId = await querySnapshot?.docs[0]?.id;
    try {
      if (likesDocId !== undefined) {
        console.log("Like exists, deleting...");
        const deleteId = doc(db, "posts", id, "likes", likesDocId);
        await deleteDoc(deleteId);
      } else {
        console.log("Like does not exist, adding...");
        await setDoc(likesRef, {
          id: user?.uid,
          name:
            userData?.name?.charAt(0)?.toUpperCase() +
            userData?.name?.slice(1) || user?.displayName?.split(" ")[0],
        });

        const formattedDate = new Date(selectedDate).toLocaleDateString();
        console.log("Formatted Date:", formattedDate);

        console.log("Adding document to 'mail' collection...");
        await addDoc(collection(db, 'mail'), {
          to: [user.email],
          message: {
            from: 'info.timevent@gmail.com',
            subject: `Participation Confirmation for ${name}`,
            text: `You have successfully registered to participate in the event hosted by ${name} on ${formattedDate}.`,
            html: `<p>You have successfully registered to participate in the event hosted by <strong>${name}</strong> on <strong>${formattedDate}</strong>.</p>`,
          },
        });
        console.log("Document added to 'mail' collection successfully.");
      }
    } catch (err) {
      alert(err.message);
      console.log("Error in handleLike:", err.message);
    }
  };

  const deletePost = async (e) => {
    e.preventDefault();
    try {
      if (user?.uid === uid) {
        await deleteDoc(singlePostDocument);
      } else {
        alert("You cant delete other users posts !!!");
      }
    } catch (err) {
      alert(err.message);
      console.log(err.message);
    }
  };

  useEffect(() => {
    const getLikes = async () => {
      try {
        const q = collection(db, "posts", id, "likes");
        await onSnapshot(q, (doc) => {
          dispatch({
            type: ADD_LIKE,
            likes: doc.docs.map((item) => item.data()),
          });
          const likersNames = doc.docs.map((item) => item.data().name);
          setLikers(likersNames);
        });
      } catch (err) {
        dispatch({ type: HANDLE_ERROR });
        alert(err.message);
        console.log(err.message);
      }
    };
    return () => getLikes();
  }, [id, ADD_LIKE, HANDLE_ERROR]);

  return (
    <div className="mb-4">
      <div className="flex flex-col py-4 bg-white rounded-t-3xl">
        <div className="flex justify-start items-center pb-4 pl-4 ">
          <Avatar
            size="sm"
            variant="circular"
            src={organizerData?.profileImage || avatar}
            alt="avatar"
          ></Avatar>

          <div className="flex flex-col ml-4">
            <p className="py-2 font-roboto font-medium text-sm text-gray-700 no-underline tracking-normal leading-none">
              {name}
            </p>
            <p className="font-roboto font-medium text-xs text-gray-500 no-underline tracking-normal leading-none whitespace-nowrap overflow-hidden text-ellipsis max-w-full">
              Published: {new Date(timestamp).toLocaleDateString()}
            </p>
          </div>
          {user && user.uid !== uid && user.role !== 'organizer' && (
            <div onClick={addUser} className="w-full flex justify-end cursor-pointer mr-10">
              <img
                className="hover:bg-blue-100 rounded-xl p-2 h-11 w-11"
                src={addFriend}
                alt="addFriend"
              />
            </div>
          )}
        </div>
        <div>
          <p className="ml-4 pb-4 font-roboto font-medium text-sm text-gray-700 no-underline tracking-normal leading-none">
            {text}
          </p>
          {image && (
            <img className="h-[500px] w-full" src={image} alt="postImage"></img>
          )}
        </div>
        <div className="ml-4 mt-4 font-roboto font-medium text-sm text-gray-500 no-underline tracking-normal leading-none">
          Event Date: {new Date(selectedDate).toLocaleDateString()}
        </div>
        <div className="flex justify-around items-center pt-4">
          <button
            className="flex items-center cursor-pointer rounded-lg p-2 hover:bg-gray-100"
            onClick={handleLike}
          >
            <img className="h-8 mr-4" src={like} alt=""></img>
            {state.likes?.length > 0 && state?.likes?.length}
          </button>
          <div
            className="flex items-center cursor-pointer rounded-lg p-2 hover:bg-gray-100"
            onClick={() => setOpenCommentSection(!openCommentSection)}
          >
            <div className="flex items-center cursor-pointer">
              <img className="h-8 mr-4" src={comment} alt="comment"></img>
              <p className="font-roboto font-medium text-md text-gray-700 no-underline tracking-normal leading-none">
                Comments
              </p>
            </div>
          </div>
          <div
            className="flex items-center cursor-pointer rounded-lg p-2 hover:bg-gray-100"
            onClick={handleOpenLikesDialog}
          >
            <div className="flex items-center cursor-pointer">
              <img className="h-8 mr-4" src={participants} alt="participants"></img>
              <p className="font-roboto font-medium text-md text-gray-700 no-underline tracking-normal leading-none">
              </p>
            </div>
          </div>
          <div className="flex items-center cursor-pointer rounded-lg p-2 hover:bg-gray-100">
            <FontAwesomeIcon icon={faMapMarkedAlt} className="h-8 mr-4" />
            {postLocation && (
              <div
                className="flex items-center cursor-pointer rounded-lg p-2 hover:bg-gray-100"
                onClick={() => setShowLocationDialog(true)}
              >
                <p className="font-roboto font-medium text-md text-gray-700 no-underline tracking-normal leading-none">
                  View Location
                </p>
              </div>
            )}
            <Dialog open={showLocationDialog} handler={toggleLocationDialog}>
              <DialogHeader>Location</DialogHeader>
              <DialogBody>
                <LocationViewer lat={location?.lat} lng={location?.lng} />
              </DialogBody>
              <DialogFooter>
                <Button variant="text" color="blue" onClick={toggleLocationDialog}>
                  <span>Close</span>
                </Button>
              </DialogFooter>
            </Dialog>
          </div>
          <div
            className="flex items-center cursor-pointer rounded-lg p-2 hover:bg-gray-100"
            onClick={deletePost}
          >
            <img className="h-8 mr-4" src={remove} alt="delete"></img>
            <p className="font-roboto font-medium text-md text-gray-700 no-underline tracking-normal leading-none">
              Delete
            </p>
          </div>
        </div>
      </div>

      <Dialog open={openLikesDialog} handler={handleOpenLikesDialog}>
        <DialogHeader>Participants</DialogHeader>
        <DialogBody>
          {likers.length > 0 ? (
            likers.map((liker, index) => (
              <p key={index} className="ml-2">
                {liker}
              </p>
            ))
          ) : (
            <p>No participant yet.</p>
          )}
        </DialogBody>
        <DialogFooter>
          <Button variant="text" color="blue" onClick={handleOpenLikesDialog}>
            <span>Close</span>
          </Button>
        </DialogFooter>
      </Dialog>

      {openCommentSection && <CommentSection postId={id}></CommentSection>}
    </div>
  );
};

export default PostCard;
