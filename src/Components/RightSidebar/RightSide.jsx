import React, { useState, useContext } from "react";
import timi from "../../assets/timi.jpg";
import { AuthContext } from "../AppContext/AppContext";
import { Link } from "react-router-dom";
import { FaStar } from 'react-icons/fa';
import { Avatar } from "@material-tailwind/react";
import avatar from "../../assets/avatar.png";
import removeIcon from "../../assets/delete.png"; // Renamed to removeIcon to avoid confusion
import {
  collection,
  doc,
  query,
  where,
  getDocs,
  arrayRemove,
  updateDoc,
} from "firebase/firestore";
import { db } from "../Firebase/firebase";

const RightSide = () => {
  const [input, setInput] = useState("");
  const { user, userData } = useContext(AuthContext);
  const friendList = userData?.friends;
  const isNormalUser = user && userData;

  const searchFriends = (data) => {
    return data.filter((item) =>
      item["name"].toLowerCase().includes(input.toLowerCase())
    );
  };

  const removeFriend = async (id, name, image) => {
    const q = query(collection(db, "users"), where("uid", "==", user?.uid));
    const getDoc = await getDocs(q);
    const userDocumentId = getDoc.docs[0].id;

    await updateDoc(doc(db, "users", userDocumentId), {
      friends: arrayRemove({ id: id, name: name, image: image }),
    });
  };

  return (
    <div className="flex flex-col h-screen bg-white shadow-lg border-2 rounded-l-xl">
      <div className="flex flex-col items-center relative pt-10">
        <img className="h-48 rounded-md" src={timi} alt="nature"></img>
      </div>
      <p className="font-roboto font-normal text-sm text-gray-700 max-w-fit no-underline tracking-normal leading-tight py-2 mx-2">
        Welcome to TimEvent! Explore the vibrant pulse of cultural and social life in Timișoara, all in one place. We are a platform dedicated to clubs, restaurants, and other exceptional venues in the city, providing them with the opportunity to showcase their events.
      </p>
      {isNormalUser && (
        <div className="mx-2 mt-10 flex flex-col h-full">
          <p className="font-roboto font-medium text-sm text-gray-700 no-underline tracking-normal leading-none">
            Favorite organizers:{" "}
          </p>
          <input
            className="border-0 outline-none mt-4"
            name="input"
            value={input}
            type="text"
            placeholder="Search organizers"
            onChange={(e) => setInput(e.target.value)}
          ></input>
          <div className="mt-4 flex-1 overflow-y-auto" style={{ maxHeight: '300px' }}>
            {friendList?.length > 0 ? (
              searchFriends(friendList)?.map((friend) => {
                return (
                  <div
                    className="flex items-center justify-between hover:bg-gray-100 duration-300 ease-in-out"
                    key={friend.id}
                  >
                    <Link to={`/profile/${friend.id}`}>
                      <div className="flex items-center my-2 cursor-pointer">
                        <div className="flex items-center">
                          <FaStar className="text-black-500" size={28} />
                          <p className="ml-4 font-roboto font-medium text-sm text-gray-700 no-underline tracking-normal leading-none">
                           {friend.name} 
                          </p>
                        </div>
                      </div>
                    </Link>
                    <div className="mr-4">
                    <img
                        onClick={() =>
                          removeFriend(friend.id, friend.name, friend.image)
                        }
                        className="cursor-pointer h-6 w-6"
                        src={removeIcon} 
                        alt="deleteFriend"
                      ></img>
                    </div>
                  </div>
                );
              })
            ) : (
              <p className="mt-10 font-roboto font-medium text-sm text-gray-700 no-underline tracking-normal leading-none">
                Add your favorite organizers to check their profile
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default RightSide;
