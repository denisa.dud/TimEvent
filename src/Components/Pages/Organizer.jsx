import React, { useState, useContext, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
} from "@material-tailwind/react";
import { Input } from "@material-tailwind/react";
import { Button } from "@material-tailwind/react";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import ClipLoader from "react-spinners/ClipLoader";
import { AuthContext } from "../AppContext/AppContext";
import peoplee from "../../assets/peoplee.png";

const Organizer = () => {
  const [loading, setLoading] = useState(false);
  const { registerOrganizerWithEmailAndPassword } = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    setLoading(true);
    
    setLoading(false);
  }, []);

  let initialValues = {
    name: "",
    email: "",
    password: "",
    kind: "", 
    address: "", 
    profileImage: null, 
  };

  const validationSchema = Yup.object({
    name: Yup.string()
      .required("Required")
      .min(4, "Must be at least 4 characters long")
      .matches(/^[a-zA-Z\s]+$/, "Name can only contain letters and spaces"),
    email: Yup.string().email("Invalid email address").required("Required"),
    password: Yup.string()
      .required("Required")
      .min(6, "Must be at least 6 characters long")
      .matches(/^[a-zA-Z]+$/, "Password can only contain letters"),
    kind: Yup.string().required("Required"), 
    address: Yup.string().required("Required"), 
    profileImage: Yup.mixed(), 
  });

  const handleRegister = (e) => {
    e.preventDefault();
    const { name, email, password, kind, address, profileImage } = formik.values;
    if (formik.isValid === true) {
      registerOrganizerWithEmailAndPassword(name, email, password, kind, address, profileImage);
      setLoading(true);
    } else {
      setLoading(false);
      alert("Check your input fields");
    }
  };

  const formik = useFormik({ initialValues, validationSchema, onSubmit: handleRegister });

  return (
    <>
      {loading ? (
        <div className="grid grid-cols-1 justify-items-center items-center h-screen">
          <ClipLoader color="#367fd6" size={150} speedMultiplier={0.5} />
        </div>
      ) : (
        <div className="bg-gradient-to-r from-gray-400 to-white min-h-screen flex">
         
          <div className="w-1/2 flex justify-center items-center">
            <div className="max-w-md w-full">
              <Card>
                <CardHeader
                  variant="gradient"
                  color="gray"
                  className="mb-4 grid h-28 place-items-center"
                >
                  <Typography variant="h3" color="white">
                    REGISTER ORGANIZER
                  </Typography>
                </CardHeader>
                <CardBody className="flex flex-col gap-4">
                  <form onSubmit={handleRegister}>
                    <div className="mb-2">
                      <Input
                        name="name"
                        type="text"
                        label="Name"
                        size="lg"
                        {...formik.getFieldProps("name")}
                      />
                    </div>
                    <div>
                      {formik.touched.name && formik.errors.name && (
                        <Typography variant="small" color="red">
                          {formik.errors.name}
                        </Typography>
                      )}
                    </div>
                    <div className="mt-4 mb-2">
                      <Input
                        name="email"
                        type="email"
                        label="Email"
                        size="lg"
                        {...formik.getFieldProps("email")}
                      />
                    </div>
                    <div>
                      {formik.touched.email && formik.errors.email && (
                        <Typography variant="small" color="red">
                          {formik.errors.email}
                        </Typography>
                      )}
                    </div>
                    <div className="mt-4 mb-2">
                      <Input
                        name="password"
                        type="password"
                        label="Password"
                        size="lg"
                        {...formik.getFieldProps("password")}
                      />
                    </div>
                    <div>
                      {formik.touched.password && formik.errors.password && (
                        <Typography variant="small" color="red">
                          {formik.errors.password}
                        </Typography>
                      )}
                    </div>
                    <div className="mt-4 mb-2">
                      <label htmlFor="kind" className="block text-sm font-medium text-gray-700">
                        Kind
                      </label>
                      <select
                        id="kind"
                        name="kind"
                        className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                        {...formik.getFieldProps("kind")}
                      >
                        <option value="">Select an option</option>
                        <option value="night_clun">Night club</option>
                        <option value="restaurant">Restaurant</option>
                        <option value="bar">Bar</option>
                        <option value="coffe_shop">Coffee Shop</option>
                        <option value="hotel">Hotel</option>
                        <option value="Other">Other</option>
                      </select>
                    </div>
                    {formik.touched.kind && formik.errors.kind && (
                      <Typography variant="small" color="red">
                        {formik.errors.kind}
                      </Typography>
                    )}
                    <div className="mt-4 mb-2">
                      <Input
                        name="address"
                        type="text"
                        label="Address"
                        size="lg"
                        {...formik.getFieldProps("address")}
                      />
                    </div>
                    <div>
                      {formik.touched.address && formik.errors.address && (
                        <Typography variant="small" color="red">
                          {formik.errors.address}
                        </Typography>
                      )}
                    </div>
                    <div className="mt-4 mb-2">
                      
                      <Input
                        name="profileImage"
                        type="file"
                        label="Profile Image"
                        size="lg"
                        onChange={(event) => formik.setFieldValue("profileImage", event.currentTarget.files[0])}
                      />
                    </div>
                    <Button
                      variant="gradient"
                      fullWidth
                      type="submit"
                      className="mb-4"
                    >
                      Register
                    </Button>
                  </form>
                </CardBody>
                <CardFooter className="pt-0">
                  <div className="mt-6 flex font-roboto text-base justify-center">
                    Already have an account?
                    <Link to="/login">
                      <p className="ml-1 font-bold font-roboto text-base text-blue-500 text-center">
                        Login
                      </p>
                    </Link>
                  </div>
                </CardFooter>
              </Card>
            </div>
          </div>
        
          <div className="w-1/2 flex justify-center items-center">
            <img src={peoplee} alt="People" className="w-full h-auto" />
          </div>
        </div>
      )}
    </>
  );
};

export default Organizer;
