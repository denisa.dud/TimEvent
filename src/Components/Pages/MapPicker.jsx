import React, { useState } from "react";
import { GoogleMap, useLoadScript, Marker } from "@react-google-maps/api";
import { Button } from "@material-tailwind/react";

const MapPicker = ({ onSelectLocation, setAddress }) => {
  const [markerPosition, setMarkerPosition] = useState(null);

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: "AIzaSyDlmzLF-y_UdrFGND3YelN0BXCoDZSyvPM",
  });

  const handleMapClick = (event) => {
    setMarkerPosition({
      lat: event.latLng.lat(),
      lng: event.latLng.lng(),
    });
    getAddressFromLatLng(event.latLng.lat(), event.latLng.lng());
  };

  const getAddressFromLatLng = (lat, lng) => {
    console.log(`Latitudine: ${lat}, Longitudine: ${lng}`);
  };

  const handleConfirmLocation = () => {
    if (markerPosition) {
      onSelectLocation(markerPosition);
    }
  };

  if (loadError) return "Error loading maps";
  if (!isLoaded) return "Loading maps";

  return (
    <div style={{ height: "200px", width: "100%", marginBottom: "20px" }}>
      <div style={{ height: "calc(100% - 10px)", width: "100%" }}>
        <GoogleMap
          mapContainerStyle={{ height: "100%", width: "100%" }}
          zoom={8}
          center={{ lat: 45.7489, lng: 21.2087 }}
          onClick={handleMapClick}
        >
          {markerPosition && <Marker position={markerPosition} />}
        </GoogleMap>
      </div>
      <div style={{ textAlign: "center" }}>
        <Button onClick={handleConfirmLocation}>Confirm Location</Button>
      </div>
    </div>
  );
  
};

export default MapPicker;
