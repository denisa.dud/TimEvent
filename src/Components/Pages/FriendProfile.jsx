import React, { useState, useEffect } from "react";
import LeftSide from "../LeftSideBar/LeftSide";
import Navbar from "../Navbar/Navbar";
import RightSide from "../RightSidebar/RightSide";
import Main from "../Main/Main";
import profilePic from "../../assets/profilePic.jpg";
import { Avatar } from "@material-tailwind/react";
import avatar from "../../assets/avatar.png";
import { collection, where, query, onSnapshot } from "firebase/firestore";
import { db } from "../Firebase/firebase";
import { useParams } from "react-router-dom";

const FriendProfile = () => {
  const { id } = useParams();
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    const getUserProfile = async () => {
      const q = query(collection(db, "organizers"), where("uid", "==", id));
      const unsubscribe = onSnapshot(q, (querySnapshot) => {
        if (!querySnapshot.empty) {
          querySnapshot.forEach((doc) => {
            setProfile(doc.data());
          });
        } else {
          setProfile(null);
        }
      });
      return () => unsubscribe();
    };

    getUserProfile();
  }, [id]);

  return (
    <div className="w-full">
      <div className="fixed top-0 z-10 w-full bg-white">
        <Navbar></Navbar>
      </div>
      <div className="flex bg-gray-100">
        <div className="flex-auto w-[20%] fixed top-12">
          <LeftSide></LeftSide>
        </div>
        <div className="flex-auto w-[60%] absolute left-[20%] top-14 bg-gray-100 rounded-xl">
          <div className="w-[80%] mx-auto">
            <div>
              <div className="relative py-4">
                <img
                  className="h-96 w-full rounded-md"
                  src={profilePic}
                  alt="profilePic"
                ></img>
                <div className="absolute bottom-10 left-6">
                  <Avatar
                    size="xl"
                    variant="circular"
                    src={profile?.profileImage || profile?.image || avatar}
                    alt="avatar"
                  ></Avatar>
                  <p className="py-2 font-roboto font-medium text-sm text-white no-underline tracking-normal leading-none">
                    {profile?.email}
                  </p>
                  <p className="py-2 font-roboto font-medium text-sm text-white no-underline tracking-normal leading-none">
                    {profile?.name}
                  </p>
                  <p className="py-2 font-roboto font-medium text-sm text-white no-underline tracking-normal leading-none">
                    Kind: {profile?.kind?.replace(/_/g, ' ')}
                  </p>
                  <p className="py-2 font-roboto font-medium text-sm text-white no-underline tracking-normal leading-none">
                    Address: {profile?.address}
                  </p>
                </div>
              </div>
            </div>
            <Main></Main>
          </div>
        </div>
        <div className="flex-auto w-[20%] fixed right-0 top-12">
          <RightSide></RightSide>
        </div>
      </div>
    </div>
  );
};

export default FriendProfile;
