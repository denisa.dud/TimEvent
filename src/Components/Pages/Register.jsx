import React, { useState, useContext, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
} from "@material-tailwind/react";
import { Input } from "@material-tailwind/react";
import { Button } from "@material-tailwind/react";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import ClipLoader from "react-spinners/ClipLoader";
import { AuthContext } from "../AppContext/AppContext";
import { auth, onAuthStateChanged } from "../Firebase/firebase";
import peoplee from "../../assets/peoplee.png";

const Register = () => {
  const [loading, setLoading] = useState(false);
  const { registerWithEmailAndPassword } = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    setLoading(true);
    onAuthStateChanged(auth, (user) => {
      if (user) {
        navigate("/");
        setLoading(false);
      } else {
        setLoading(false);
      }
    });
  }, [navigate]);

  let initialValues = {
    name: "",
    email: "",
    password: "",
    profileImage: null, 
  };

  const validationSchema = Yup.object({
    name: Yup.string()
      .required("Required")
      .min(4, "Must be at least 4 characters long")
      .matches(/^[a-zA-Z\s]+$/, "Name can only contain letters and spaces"),
    email: Yup.string().email("Invalid email address").required("Required"),
    password: Yup.string()
      .required("Required")
      .min(6, "Must be at least 6 characters long")
      .matches(/^[a-zA-Z]+$/, "Password can only contain letters"),
    profileImage: Yup.mixed(), 
  });

  const handleRegister = (values) => {
    registerWithEmailAndPassword(values.name, values.email, values.password, values.profileImage);
  };

  const formik = useFormik({ initialValues, validationSchema, onSubmit: handleRegister });

  return (
    <>
      {loading ? (
        <div className="grid grid-cols-1 justify-items-center items-center h-screen">
          <ClipLoader color="#367fd6" size={150} speedMultiplier={0.5} />
        </div>
      ) : (
        <div className="bg-gradient-to-r from-gray-400 to-white min-h-screen flex">
       
          <div className="w-1/2 flex justify-center items-center">
            <div className="max-w-md w-full">
              <Card>
                <CardHeader
                  variant="gradient"
                  color="gray"
                  className="mb-4 grid h-28 place-items-center"
                >
                  <Typography variant="h3" color="white">
                    REGISTER
                  </Typography>
                </CardHeader>
                <CardBody className="flex flex-col gap-4">
                  <form onSubmit={formik.handleSubmit}>
                    <div className="mb-2">
                      <Input
                        name="name"
                        type="text"
                        label="Name"
                        size="lg"
                        {...formik.getFieldProps("name")}
                      />
                    </div>
                    <div>
                      {formik.touched.name && formik.errors.name && (
                        <Typography variant="small" color="red">
                          {formik.errors.name}
                        </Typography>
                      )}
                    </div>
                    <div className="mt-4 mb-2">
                      <Input
                        name="email"
                        type="email"
                        label="Email"
                        size="lg"
                        {...formik.getFieldProps("email")}
                      />
                    </div>
                    <div>
                      {formik.touched.email && formik.errors.email && (
                        <Typography variant="small" color="red">
                          {formik.errors.email}
                        </Typography>
                      )}
                    </div>
                    <div className="mt-4 mb-2">
                      <Input
                        name="password"
                        type="password"
                        label="Password"
                        size="lg"
                        {...formik.getFieldProps("password")}
                      />
                      <div>
                        {formik.touched.password && formik.errors.password && (
                          <Typography variant="small" color="red">
                            {formik.errors.password}
                          </Typography>
                        )}
                      </div>
                    </div>
                    <div className="mt-4 mb-2">
                    
                      <Input
                        name="profileImage"
                        type="file"
                        label="Profile Image"
                        size="lg"
                        onChange={(event) => formik.setFieldValue("profileImage", event.currentTarget.files[0])}
                      />
                    </div>
                    <Button
                      variant="gradient"
                      fullWidth
                      type="submit"
                      className="mb-4"
                    >
                      Register
                    </Button>
                  </form>
                </CardBody>
                <CardFooter className="pt-0">
                  <div className="mt-6 flex font-roboto text-base justify-center">
                    Already have an account?
                    <Link to="/login">
                      <p className="ml-1 font-bold font-roboto text-base text-blue-500 text-center">
                        Login
                      </p>
                    </Link>
                  </div>
                </CardFooter>
              </Card>
            </div>
          </div>
          
          <div className="w-1/2 flex justify-center items-center">
            <img src={peoplee} alt="People" className="w-full h-auto" />
          </div>
        </div>
      )}
    </>
  );
};

export default Register;
