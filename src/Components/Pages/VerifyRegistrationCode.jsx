import React, { useState } from "react";
import { Input } from "@material-tailwind/react";
import { Button } from "@material-tailwind/react";
import { Typography } from "@material-tailwind/react";
import { useNavigate } from "react-router-dom";
import peoplee from "../../assets/peoplee.png";

const VerifyRegistrationCode = () => {
  const [code, setCode] = useState("");
  const navigate = useNavigate();

  const handleVerifyCode = () => {
    if (code === "1234") {
      navigate("/organizer");
    } else {
      alert("Invalid code. Please try again.");
    }
  };

  return (
    <div className="bg-gradient-to-r from-gray-400 to-white min-h-screen flex">

      <div className="w-1/2 flex justify-center items-center">
        <div className="max-w-md w-full">
          <Typography variant="h6" color="blue-gray" className="pb-4">
            Enter the registration code provided to you to verify your organizer account.
          </Typography>
          <Input
            name="code"
            type="text"
            label="Registration Code"
            value={code}
            onChange={(e) => setCode(e.target.value)}
          ></Input>
          <Button variant="gradient" fullWidth className="mt-4" onClick={handleVerifyCode}>
            Continue
          </Button>
        </div>
      </div>
     
      <div className="w-1/2 flex justify-center items-center">
        <img src={peoplee} alt="People" className="w-full h-auto" />
      </div>
    </div>
  );
};

export default VerifyRegistrationCode;
