import React, { useContext } from 'react';
import { AuthContext } from '../AppContext/AppContext';
import { useNavigate } from 'react-router-dom';
import peoplee from "../../assets/peoplee.png";
import background from "../../assets/backround.png"; 
import { FaCalendarAlt, FaComments, FaMapMarkerAlt, FaPlusCircle, FaBullhorn, FaPhone, FaEnvelope, FaInstagram } from 'react-icons/fa';

const Welcome = () => {
  const { signInWithGoogle } = useContext(AuthContext);
  const navigate = useNavigate();

  return (
    <div>
      <style jsx>{`
        @keyframes sparkle {
          0%, 100% {
            text-shadow: 0 0 5px #fff, 0 0 10px #e3dfd5, 0 0 15px #e3dfd5, 0 0 20px #e3dfd5, 0 0 25px #e3dfd5, 0 0 30px #e3dfd5, 0 0 35px #e3dfd5;
            color: gray-700;
          }
          50% {
            text-shadow: 0 0 10px #fff, 0 0 20px #e3dfd5, 0 0 30px #e3dfd5, 0 0 40px #e3dfd5, 0 0 50px #e3dfd5, 0 0 60px #e3dfd5, 0 0 70px #e3dfd5;
            color: #fff;
          }
        }

        .sparkle-text {
          display: inline-block;
          animation: sparkle 1.5s infinite;
        }

        .card {
          background: linear-gradient(135deg, #e0e0e0 0%, #c0c0c0 100%);
          transition: transform 0.3s, box-shadow 0.3s;
          position: relative;
          overflow: hidden;
          padding: 20px;
          display: flex;
          flex-direction: column;
          align-items: center;
          text-align: center;
        }

        .card:hover {
          transform: translateY(-10px);
          box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
        }

        .card-icon {
          font-size: 3rem;
          color: #555;
          margin-bottom: 10px;
        }

        .card-content {
          z-index: 1;
          position: relative;
        }

        .section-bg {
          background: url(${background}) no-repeat center center;
          background-size: cover;
        }

        .contact-section {
          background: #f5f5f5;
          padding: 20px;
          text-align: center;
        }

        .contact-info {
          display: flex;
          flex-direction: column;
          align-items: center;
          gap: 10px;
        }

        .contact-info div {
          display: flex;
          align-items: center;
          gap: 10px;
          font-size: 1.2rem;
        }

        .contact-icon {
          color: #333;
          font-size: 1.5rem;
        }
      `}</style>

      {/* Navbar */}
      <div className="bg-gradient-to-r from-gray-400 to-white h-16 flex items-center justify-between px-4">
        <div className="text-3xl font-extrabold font-roboto">
          <span className="bg-clip-text text-transparent bg-gradient-to-r from-gray-500 to-gray-800">
            TimEvent
          </span>
        </div>
        <button
          className="bg-gray-800 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded"
          onClick={() => navigate('/login')}
        >
          SIGN IN
        </button>
      </div>
      
      <hr className="border-black w-full border-t-1" />
      
      <div className="bg-gradient-to-r from-gray-400 to-white min-h-screen flex justify-center items-center">
        <div className="max-w-md mx-auto flex-grow overflow-y-auto flex-col flex justify-center px-4 sm:max-w-3xl ml-10">
          <h1 className="text-5xl text-black mb-0 text-left"><b>Where every occasion sparkles,</b></h1>
          <h1 className="text-3xl text-gray-700 mb-4 text-left">
            <b><span className="sparkle-text">TimEvent</span> lights the way!</b>
          </h1>
          <h1 className="text-1xl text-gray-600 mb-4 text-left">
            TimEvent lights the way to unforgettable experiences in Timișoara! Post and discover the most captivating events in our vibrant city.
          </h1>
        </div>
      
        <div className="w-1/2 flex justify-end">
          <img src={peoplee} alt="People" className="w-full h-auto" />
        </div>
      </div>

      {/* Client Section */}
      <div className="section-bg min-h-screen flex justify-center items-center">
        <div className="container mx-auto px-4 py-8">
          <h2 className="text-3xl font-bold text-center mb-8">Do you want to keep up with everything that's going on in Timișoara? Start here!</h2>
          <div className="flex justify-center mb-8">
            <button
              className="bg-gray-900 hover:bg-gray text-white font-bold py-2 px-4 rounded"
              onClick={() => navigate('/register')}
            >
              SIGN UP
            </button>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <div className="card rounded-lg shadow-lg">
              <FaCalendarAlt className="card-icon" />
              <div className="card-content">
                <h3 className="text-xl font-bold mb-2">Discover Events</h3>
                <p>Stay updated with the latest events happening around you.</p>
              </div>
            </div>
            <div className="card rounded-lg shadow-lg">
              <FaComments className="card-icon" />
              <div className="card-content">
                <h3 className="text-xl font-bold mb-2">Interact with Posts</h3>
                <p>Participate and comment on events you are interested in.</p>
              </div>
            </div>
            <div className="card rounded-lg shadow-lg">
              <FaMapMarkerAlt className="card-icon" />
              <div className="card-content">
                <h3 className="text-xl font-bold mb-2">Check Event Calendar</h3>
                <p>View the schedule of upcoming events in your area.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Organizer Section */}
      <div className="section-bg min-h-screen flex justify-center items-center">
        <div className="container mx-auto px-4 py-8">
          <h2 className="text-3xl font-bold text-center mb-8">Want to host and promote amazing events in Timișoara? Start here!</h2>
          <div className="flex justify-center mb-8">
            <button
              className="bg-gray-900 hover:bg-gray text-white font-bold py-2 px-4 rounded"
              onClick={() => navigate('/verify')}
            >
              SIGN UP AS ORGANIZER
            </button>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <div className="card rounded-lg shadow-lg">
              <FaPlusCircle className="card-icon" />
              <div className="card-content">
                <h3 className="text-xl font-bold mb-2">Create Events</h3>
                <p>Post and manage your events easily.</p>
              </div>
            </div>
            <div className="card rounded-lg shadow-lg">
              <FaMapMarkerAlt className="card-icon" />
              <div className="card-content">
                <h3 className="text-xl font-bold mb-2">Share Event Location</h3>
                <p>Provide details about the venue and location of your events.</p>
              </div>
            </div>
            <div className="card rounded-lg shadow-lg">
              <FaBullhorn className="card-icon" />
              <div className="card-content">
                <h3 className="text-xl font-bold mb-2">Reach Audience</h3>
                <p>Promote your events to a larger audience.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Contact Section */}
      <div className="contact-section">
        <h2 className="text-3xl font-bold mb-4">Contact Us</h2>
        <div className="contact-info">
          <div>
            <FaPhone className="contact-icon" />
            <span>0756060502</span>
          </div>
          <div>
            <FaEnvelope className="contact-icon" />
            <span>info.timevent@gmail.com</span>
          </div>
          <div>
            <FaInstagram className="contact-icon" />
            <span>timevent</span>
          </div>
        </div>
        <p className="mt-4 text-gray-700">
        If you are an organizer looking to promote through the TimEvent application, please contact us to receive the registration code.
        </p>
      </div>
    </div>
  );
};

export default Welcome;
