import React from "react";
import Home from "./Home";
import { Routes, Route } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import VerifyRegistrationCode from "./VerifyRegistrationCode";
import FriendProfile from "./FriendProfile";
import Welcome from "./Welcome";
import Organizer from "./Organizer";
import Calendar from "../LeftSideBar/Calendar";
import LocationViewer from "../Main/LocationViewer";




const Pages = () => {
  return (
    <div>
      <Routes>
        <Route path="/welcome" element={<Welcome></Welcome>}></Route>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/login" element={<Login></Login>}></Route>
        <Route path="/register" element={<Register></Register>}></Route>
        <Route path="verify" element={<VerifyRegistrationCode></VerifyRegistrationCode>}></Route>
        <Route path="/organizer" element={<Organizer></Organizer>}></Route>
        <Route path="/calendar" element={<Calendar></Calendar>}></Route>
        <Route path="/location-view" element={<LocationViewer></LocationViewer>}></Route>
        <Route
          path="/profile/:id"
          element={<FriendProfile></FriendProfile>}
        ></Route>
      </Routes>
    </div>
  );
};

export default Pages;
