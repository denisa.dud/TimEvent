import React, { createContext, useState, useEffect } from "react";
import {
  GoogleAuthProvider,
  signInWithPopup,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
  signOut,
} from "firebase/auth";
import { auth, db, onAuthStateChanged } from "../Firebase/firebase";
import {
  doc,
  query,
  where,
  collection,
  getDoc,
  getDocs,
  addDoc,
  onSnapshot,
  updateDoc, 
} from "firebase/firestore";
import { useNavigate } from "react-router-dom";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage } from "../Firebase/firebase";


export const AuthContext = createContext();

const AppContext = ({ children }) => {
  const collectionUsersRef = collection(db, "users");
  const collectionOrganizersRef = collection(db, "organizers");
  const provider = new GoogleAuthProvider();
  const [user, setUser] = useState();
  const [userData, setUserData] = useState();
  const [organizerData, setOrganizerData] = useState();

  

  const navigate = useNavigate();


  const signInWithGoogle = async () => {
    try {
      const popup = await signInWithPopup(auth, provider);
      const user = popup.user;
      const q = query(collectionUsersRef, where("uid", "==", user.uid));
      const docs = await getDocs(q);
      if (docs.docs.length === 0) {
        await addDoc(collectionUsersRef, {
          uid: user?.uid,
          name: user?.displayName,
          email: user?.email,
          image: user?.photoURL,
          authProvider: popup?.providerId,
        });
      }
    } catch (err) {
      alert(err.message);
      console.log(err.message);
    }
  };

  const loginWithEmailAndPassword = async (email, password) => {
    try {
      await signInWithEmailAndPassword(auth, email, password);
    } catch (err) {
      alert(err.message);
      console.log(err.message);
    }
  };

  const registerWithEmailAndPassword = async (name, email, password, profileImage) => {
    try {
      const res = await createUserWithEmailAndPassword(auth, email, password);
      const user = res.user;
      
      
      let imageURL = null;
      if (profileImage) {
        const storageRef = ref(storage, `profile_images/${user.uid}/${profileImage.name}`);
        await uploadBytes(storageRef, profileImage);
        imageURL = await getDownloadURL(storageRef);
      }
  
      
      const collectionRef = user.uid.startsWith("organizer") ? collection(db, "organizers") : collection(db, "users");
      await addDoc(collectionRef, {
        uid: user.uid,
        name,
        email: user.email,
        profileImage: imageURL, 
      });
    } catch (err) {
      alert(err.message);
      console.log(err.message);
    }
  };
  



  const signOutUser = async () => {
    await signOut(auth);
  };

  const registerOrganizerWithEmailAndPassword = async (name, email, password, kind, address, profileImage, location) => {
    try {
      const res = await createUserWithEmailAndPassword(auth, email, password);
      const user = res.user; 
      let imageURL = null;
      if (profileImage) {
        const storageRef = ref(storage, `profile_images/${user.uid}/${profileImage.name}`);
        await uploadBytes(storageRef, profileImage);
        imageURL = await getDownloadURL(storageRef);
      }
    
  
      await addDoc(collectionOrganizersRef, {
        uid: user.uid,
        name,
        email: user.email,
        kind,
        address,
        profileImage: imageURL, 
      });
    } catch (err) {
      alert(err.message);
      console.log(err.message);
    }
  };
  
  

  const userStateChanged = async () => {
    onAuthStateChanged(auth, async (user) => {
      if (user) {
        const q = query(collectionUsersRef, where("uid", "==", user?.uid));
        await onSnapshot(q, (doc) => {
          setUserData(doc?.docs[0]?.data());
        });

        const organizerQ = query(collectionOrganizersRef, where("uid", "==", user?.uid)); 
        await onSnapshot(organizerQ, (doc) => {
          setOrganizerData(doc?.docs[0]?.data()); 
        });

        setUser(user);
      } else {
        setUser(null);
        navigate("/welcome");
      }
    });
  };



  useEffect(() => {
    userStateChanged();
    if (user || userData || organizerData) { 
      navigate("/");
    } else {
      navigate("/login");
    }
    return () => userStateChanged();
  }, []);

  

  const initialState = {
    signInWithGoogle: signInWithGoogle,
    loginWithEmailAndPassword: loginWithEmailAndPassword,
    registerWithEmailAndPassword: registerWithEmailAndPassword,
    registerOrganizerWithEmailAndPassword: registerOrganizerWithEmailAndPassword,
    signOutUser: signOutUser,
    user: user,
    userData: userData,
  };

  
  

  return (
    <div>
      <AuthContext.Provider value={initialState}>
        {children}
      </AuthContext.Provider>
    </div>
  );
};

export default AppContext;