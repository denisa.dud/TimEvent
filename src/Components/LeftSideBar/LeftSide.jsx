import React, { useContext, useState, useEffect } from "react";
import { Tooltip, Avatar, Button } from "@material-tailwind/react";
import { FaMapMarkerAlt, FaBriefcase } from "react-icons/fa";
import nature from "../../assets/nature.jpg";
import avatar from "../../assets/avatar.png";
import { AuthContext } from "../AppContext/AppContext";
import { Link } from "react-router-dom";
import { collection, query, where, onSnapshot } from "firebase/firestore";
import { db } from "../Firebase/firebase";
import leftdes from "../../assets/leftdes.png"

const LeftSide = () => {
  const { user, userData } = useContext(AuthContext);
  const [organizerData, setOrganizerData] = useState({});

  useEffect(() => {
    if (user) {
      const unsubscribe = onSnapshot(
        query(collection(db, "organizers"), where("uid", "==", user.uid)),
        (snapshot) => {
          snapshot.forEach((doc) => {
            const data = doc.data();
            
            setOrganizerData(data);
          });
        }
      );

      return () => {
        unsubscribe();
      };
    }
  }, [user]);

  return (
    <div className="flex flex-col h-screen bg-white pb-4 border-2 rounded-r-xl shadow-lg">
      <div className="flex flex-col items-center relative">
        <img
          className="h-28 w-full rounded-r-xl"
          src={nature}
          alt="nature"
        ></img>
        <div className="absolute -bottom-4">
          <Tooltip content="Profile" placement="top">
            <Avatar size="md" src={organizerData?.profileImage || userData?.profileImage || user?.photoURL || avatar} alt="avatar" />
          </Tooltip>
        </div>
      </div>
      <div className="flex flex-col items-center pt-6 pb-4">
        <p className="font-roboto font-medium text-md text-gray-700 no-underline tracking-normal leading-none">
          {organizerData?.name || userData?.name}
        </p>
      </div>
      <div className="flex flex-col pl-2">
        <div className="flex items-center pb-4">
          {organizerData.address && (
            <>
              <FaMapMarkerAlt className="h-7 w-7 text-brown-300" />
              <p className="text-base text-gray-700 no-underline tracking-normal leading-none address pl-2">
                {organizerData.address}
              </p>
            </>
          )}
        </div>
        <div className="flex items-center">
          {organizerData.kind && (
            <>
              <FaBriefcase className="h-7 w-7 text-brown-300" />
              <p className="text-base text-gray-700 font-roboto no-underline tracking-normal leading-none kind pl-2">
                {organizerData.kind.replace(/_/g, ' ')}
              </p>
            </>
          )}
        </div>
        <Link to="/calendar" className="flex flex-col pl-2 w-full">
          <Button
            color="gray"
            buttonType="filled"
            size="regular"
            rounded={false}
            block={false}
            iconOnly={false}
            ripple="light"
            className="mt-4 bg-gray-500 w-11/12"
          >
            Calendar
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default LeftSide;
