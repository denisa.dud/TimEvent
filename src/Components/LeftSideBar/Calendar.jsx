import React, { useState, useEffect } from 'react';
import { collection, query, orderBy, onSnapshot } from 'firebase/firestore';
import { db } from '../Firebase/firebase';
import { Calendar as BigCalendar, momentLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import moment from 'moment';
import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
} from "@material-tailwind/react";

const localizer = momentLocalizer(moment);

const Calendar = () => {
  const [events, setEvents] = useState([]);
  const [selectedEvent, setSelectedEvent] = useState(null);
  const [dialogOpen, setDialogOpen] = useState(false);

  useEffect(() => {
    const unsubscribe = onSnapshot(query(collection(db, 'posts'), orderBy('timestamp', 'desc')), snapshot => {
      const newEvents = snapshot.docs.map(doc => {
        const data = doc.data();
        if (data && data.selectedDate) {
          return {
            id: doc.id,
            title: data.text,
            organizer: data.name,
            address: data.address,
            start: data.selectedDate.toDate(),
            end: data.selectedDate.toDate(),
            allDay: true,
            data: data
          };
        }
        return null;
      }).filter(event => event !== null);

      setEvents(newEvents);
    });

    return () => unsubscribe();
  }, []);

  const handleEventClick = event => {
    setSelectedEvent(event);
    setDialogOpen(true);
  };

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  return (
    <div className="h-full w-full">
      <h1 className="text-3xl font-semibold text-center mb-4">Calendar</h1>
      <div className="overflow-x-auto h-full w-full">
        <BigCalendar
          localizer={localizer}
          events={events}
          startAccessor="start"
          endAccessor="end"
          defaultView="month"
          address="address"
          views={['month', 'week', 'day']}
          style={{
            height: '100vh', 
            width: '100%', 
            backgroundColor: 'white',
            color: 'black',
          }}
          eventPropGetter={(event, start, end, isSelected) => ({
            style: {
              backgroundColor: '#616161',
              color: 'white',
            },
          })}
          onSelectEvent={handleEventClick}
        />
      </div>
      {selectedEvent && (
        <Dialog
          open={dialogOpen}
          onClose={handleCloseDialog}
          size="md"
        >
          <DialogHeader>Event details:</DialogHeader>
          <DialogBody>
            <p>{selectedEvent.title}</p>
            <p>{selectedEvent.organizer && `Organizer: ${selectedEvent.organizer}`}</p>
            <p>{selectedEvent.address && `Address: ${selectedEvent.address}`}</p>
            <p>Data: {selectedEvent.start && moment(selectedEvent.start).format('LL')}</p>
          </DialogBody>
          <DialogFooter>
            <Button
              variant="text"
              color="red"
              onClick={handleCloseDialog}
              className="mr-1"
            >
              <span>Close</span>
            </Button>
          </DialogFooter>
        </Dialog>
      )}
    </div>
  );
};

export default Calendar;
