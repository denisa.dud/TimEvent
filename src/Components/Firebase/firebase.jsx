
import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";
import {getAuth, onAuthStateChanged} from "firebase/auth";
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyDdx69pDSaxiFYSczYlA3eSH2pMMcvPMNw",
  authDomain: "media-40bd1.firebaseapp.com",
  projectId: "media-40bd1",
  storageBucket: "media-40bd1.appspot.com",
  messagingSenderId: "878305946470",
  appId: "1:878305946470:web:017983a74361d46bae76a7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app);

export { auth, db, storage, onAuthStateChanged };