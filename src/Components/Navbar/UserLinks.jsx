import React, { useContext, useEffect, useState } from "react";
import { Tooltip } from "@material-tailwind/react";
import { Avatar } from "@material-tailwind/react";
import avatar from "../../assets/avatar.png";
import { Link } from "react-router-dom"; 
import { AuthContext } from "../AppContext/AppContext";
import { collection, query, where, onSnapshot } from "firebase/firestore";
import { db } from "../Firebase/firebase";
import notif from "../../assets/notifi.jpg"


const UserLinks = () => {
  const { signOutUser, user, userData } = useContext(AuthContext);
  const [organizerData, setOrganizerData] = useState({});

  useEffect(() => {
    if (user) {
      const unsubscribe = onSnapshot(
        query(collection(db, "organizers"), where("uid", "==", user.uid)),
        (snapshot) => {
          snapshot.forEach((doc) => {
            const data = doc.data();
            console.log("Organizer data:", data); 
            setOrganizerData(data);
          });
        }
      );

      return () => {
        unsubscribe();
      };
    }
  }, [user]);

  return (
    <div className="flex justify-center items-center cursor-pointer">
       
  
      <div className="mx-4 flex items-center" onClick={signOutUser}>
        <Tooltip content="Sign Out" placement="bottom">
          {userData && userData.profileImage ? (
            <Avatar src={userData.profileImage} size="sm" alt="avatar" />
          ) : (
            <Avatar src={ organizerData?.profileImage || userData?.profileImage || user?.photoURL || avatar} size="sm" alt="avatar" />
          )}
        </Tooltip>
        <p className="ml-4 font-roboto text-sm text-black font-medium no-underline">
          {userData?.name || organizerData?.name}
        </p>
      </div>
      
    </div>
  );
};

export default UserLinks;
