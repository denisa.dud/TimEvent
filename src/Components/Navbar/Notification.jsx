import React from "react";

const Notification = ({ organizerName }) => {
  return (
    <div className="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-4" role="alert">
      <strong className="font-bold">Eveniment nou postat!</strong>
      <span className="block sm:inline"> Un eveniment nou a fost postat de către {organizerName}.</span>
    </div>
  );
};

export default Notification;
