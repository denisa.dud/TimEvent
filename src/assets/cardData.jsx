import card1 from "../assets/Card1.png";
import card2 from "../assets/Card2.png";
import card3 from "../assets/Card3.png";
import card4 from "../assets/Card4.png";
import card5 from "../assets/Card5.png";

export const cardData = [
  {
    id: "1",
    image: card1,
    
  },
  {
    id: "2",
    image: card2,
  },
  {
    id: "3",
    image: card3,
  },
  {
    id: "4",
    image: card4,
  },
  {
    id: "5",
    image: card5,
  },
];